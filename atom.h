#ifndef ATOM_INCLUDED
#define ATOM_INCLUDED

      int   Atom_length (const char *str);
const char *Atom_new    (const char *str, int len);
const char *Atom_string (const char *str);
const char *Atom_int    (long n);

#endif

/*
 Checked runtime errors:
 - Pass null pointer to any function in this interface
 - Pass a negative len to Atom_new
 - Pass a pointer that is not an atom to Atom_length

 Unchecked runtime errors:
 - Modify the bytes pointed to by an atom.

 Exceptions:
 - Atom_new, Atom_string, Atom_int can each raise Mem_Failed exception.
 */